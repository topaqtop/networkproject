$(function(){
   
    $.ajax({
        url: 'report.php',
        type: 'get',
        dataType: 'text',
        data: {},
        success: function(result){
           
           var a = $('#A_Report').DataTable({"lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]});
           var b = $('#B_Report').DataTable({"lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]});
           var c = $('#C_Report').DataTable({"lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]});
           var d = $('#D_Report').DataTable({"lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]});
           var general = $('#General_Report').DataTable();
           var allData = $.parseJSON(result);
            $.each(allData, function(count, value){
                if(value.call_by == "A"){
                var rowDataA = [];
                rowDataA.push(count+1);
                rowDataA.push(value.mast);
                rowDataA.push(value.call_status);
                rowDataA.push(value.call_by);
                rowDataA.push(value.ifSwitch);
                rowDataA.push(value.created_at);
                a.row.add(rowDataA);
                }

                if(value.call_by == "B"){
                    var rowDataB = [];
                    rowDataB.push(count+1);
                    rowDataB.push(value.mast);
                    rowDataB.push(value.call_status);
                    rowDataB.push(value.call_by);
                    rowDataB.push(value.ifSwitch);
                    rowDataB.push(value.created_at);
                    b.row.add(rowDataB);
                    }

                    if(value.call_by == "C"){
                        var rowDataC = [];
                        rowDataC.push(count+1);
                        rowDataC.push(value.mast);
                        rowDataC.push(value.call_status);
                        rowDataC.push(value.call_by);
                        rowDataC.push(value.ifSwitch);
                        rowDataC.push(value.created_at);
                        c.row.add(rowDataC);
                        }

                        if(value.call_by == "D"){
                            var rowDataD = [];
                            rowDataD.push(count+1);
                            rowDataD.push(value.mast);
                            rowDataD.push(value.call_status);
                            rowDataD.push(value.call_by);
                            rowDataD.push(value.ifSwitch);
                            rowDataD.push(value.created_at);
                            d.row.add(rowDataD);
                            }

                            var rowData = [];
                            rowData.push(count+1);
                            rowData.push(value.mast);
                            rowData.push(value.call_status);
                            rowData.push(value.call_by);
                            rowData.push(value.ifSwitch);
                            rowData.push(value.created_at);
                            general.row.add(rowData);
            });

            a.draw();
            b.draw();
            c.draw();
            d.draw();
            general.draw();
        
        },
        error: function(error){
            alert(error);
        }
    });
});
