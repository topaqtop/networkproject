<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Network Congestion</title>
    <link rel="stylesheet" href="../PROJECT/datatables/css/jquery.dataTables.css" type="text/css">
    <style>
    .viewrep{
        background-color: green;
    color: white;
    border: none;
    padding: 6px;
    width: 177px;
    cursor: pointer;
    }
    </style>
</head>
<body style="background-color: khaki;">
<a href="implementation.php"><button type="button" class="viewrep" style="" >Back</button></a>
<a onclick="clearData();"><button type="button" class="viewrep" style="" >Clear Record</button></a>

  <div align="center"><h2>Congestion Report</h2></div>

   <div style="display: inline-block; width: 40%;margin-left: 2%;margin-bottom: 20px;">
   <div align="center"><h4>Report for User A</h4></div>
        <table width="100%" id="A_Report" class="table datatable cell-border stripe">
        <thead>
        <tr>
        <th>S/N</th><th>Mast</th><th>Status</th><th>Caller</th><th>ifSwitch</th><th>Created_at</th>
        </tr>
        </thead>
        <tbody>
        
        </tbody>
        </table>
   </div>

   <div style="display: inline-block; width: 40%;margin-left: 15%;margin-bottom: 20px;" align="right">
   <div align="center"><h4>Report for User B</h4></div>
        <table width="100%" id="B_Report" class="table datatable cell-border stripe">
        <thead>
        <tr>
        <th>S/N</th><th>Mast</th><th>Status</th><th>Caller</th><th>ifSwitch</th><th>Created_at</th>
        </tr>
        </thead>
        <tbody>
        
        </tbody>
        </table>
    </div>

    <div style="display: inline-block; width: 40%;margin-left: 2%;margin-bottom: 20px;">
   <div align="center"><h4>Report for User C</h4></div>
        <table width="100%" id="C_Report" class="table datatable cell-border stripe">
        <thead>
        <tr>
        <th>S/N</th><th>Mast</th><th>Status</th><th>Caller</th><th>ifSwitch</th><th>Created_at</th>
        </tr>
        </thead>
        <tbody>
        
        </tbody>
        </table>
    </div>

    <div style="display: inline-block; width: 40%;margin-left: 15%;margin-bottom: 20px;" align="right">
   <div align="center"><h4>Report for User D</h4></div>
        <table width="100%" id="D_Report" class="table datatable cell-border stripe">
        <thead>
        <tr>
        <th>S/N</th><th>Mast</th><th>Status</th><th>Caller</th><th>ifSwitch</th><th>Created_at</th>
        </tr>
        </thead>
        <tbody>
        
        </tbody>
        </table>
    </div>

    <div style="display: inline-block; width: 100%;" align="right">
   <div align="center"><h4>General Report</h4></div>
        <table width="100%" id="General_Report" class="table datatable cell-border stripe">
        <thead>
        <tr>
        <th>S/N</th><th>Mast</th><th>Status</th><th>Caller</th><th>ifSwitch</th><th>Created_at</th>
        </tr>
        </thead>
        <tbody>
        
        </tbody>
        </table>
    </div>

    <script src="../PROJECT/jquery.js"></script>
    <script src="../PROJECT/datatables/js/jquery.dataTables.js"></script>
    <script src="../PROJECT/report.js">
    
    </script>
    <script>
    function clearData()
    {
        $.ajax({
                    url: "cleardata.php",
                    type: 'post',
                    data:{},
                    success: function(result){
                       alert('Report Cleared');
                       location.reload();
                    },
                    error: function(error){
                        console.log('Could not log to database. '+error);
                    }
                });
    }
    </script>

</body></html>