<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Network Congestion</title>
    <style>
    .viewrep{
        background-color: green;
    color: white;
    border: none;
    padding: 6px;
    width: 177px;
    cursor: pointer;
    }

    .mastLabel{
        color: red;
    }

    .blink_me {
  animation: blinker 1s linear infinite;
  color: red;
}

@keyframes blinker {
  50% {
    opacity: 0;
  }
}

.blink_info {
    
    animation: blinker 1s steps(5, start) infinite;
  color: red;
  font-size: 30px;
}

    .btnColor{
        background-color: red
    }

    .btnCalling{
        background-color: forestgreen;
    }

    .btns{
    border: none;
    padding: 20px;
    font-size: 15px;
    margin: 20px;
    border-radius: 15px;
    color: white;
    cursor: pointer;
    }
    </style>
</head>
<body>
<a href="viewreport.php"><button type="button" class="viewrep" style="" >View Report</button></a>
<a href="index.php"><button type="button" class="viewrep" style="" >Back</button></a>
    <div class="buttons" style="" align="center">

    <button type="button" class="btns btnColor" style="">A</button>

    <button type="button" class="btns btnColor" style="">B</button>

    <button type="button" class="btns btnColor" style="">C</button>

    <button type="button" class="btns btnColor" style="">D</button>

    <button type="button" class="btns btnColor" style="">E</button>


    <span class="blink_info" style="color: red;font-weight: bold;"></span>
    </div>

    <div style="display: inline-block;width: 20%;margin-left: 10%;">

    <img src="images/project image1.jpg" width="60px">
    <div align="left" class="mastLabel">User A</div>
    <div class="A1 AllA" style="position: absolute; margin-left: 158px; margin-top: -27px; transform: rotate(7deg); display: none;">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    </div>
    <div class="A2 AllA" style="position: absolute; margin-left: 158px; margin-top: 26px; transform: rotate(19deg); display: none;">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    </div><br>
    <img src="images/project image1.jpg" width="60px">
    <div align="left" class="mastLabel">User B</div>
    <div class="B1 AllB" style="position: absolute; margin-left: 158px; margin-top: -72px; transform: rotate(0deg); display: none;">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    </div>
    <div class="B2 AllB" style="position: absolute; margin-left: 158px; margin-top: -17px; transform: rotate(12deg); display: none;">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    </div><br>
    <img src="images/project image1.jpg" width="60px">
    <div align="left" class="mastLabel">User C</div>
    <div class="C1 AllC" style="position: absolute;margin-left: 158px;margin-top: -156px;transform: rotate(-11deg); display: none;">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    </div>
    <div class="C2 AllC" style="position: absolute; margin-left: 158px; margin-top: -93px; transform: rotate(0deg); display: none;">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    </div><br>
    <img src="images/project image1.jpg" width="60px">
    <div align="left" class="mastLabel">User D</div>
    <div class="D1 AllD" style="position: absolute;margin-left: 158px;margin-top: -208px;transform: rotate(-24deg); display: none;">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    </div>
    <div class="D2 AllD" style="position: absolute; margin-left: 158px; margin-top: -138px; transform: rotate(-7deg); display: none;">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    </div><br>
    <img src="images/project image1.jpg" width="60px">
    <div align="left" class="mastLabel">User E</div>
    <div class="E1 AllE" style="position: absolute;margin-left: 158px;margin-top: -256px;transform: rotate(-28deg);display: none;">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    </div>
    <div class="E2 AllE" style="position: absolute;margin-left: 158px;margin-top: -193px;transform: rotate(-16deg);display: none;">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    <img src="images/arrow.gif" width="60px" style="" id="">
    </div><br>
    </div>
    <div style="display: inline-block;width: 10%;margin-left: 44%;position:  absolute;margin-top: 97px;">
    <img src="images/project mast.jpg" width="100px"><div align="center" class="mastLabel maststatus1">Mast 1 Available</div><br><br>
    <img src="images/project mast.jpg" width="100px"><div align="center" class="mastLabel maststatus2">Mast 2 Available</div><br><br>
    </div>
    <div style="display: inline-block;width: 10%;margin-left: 0%;position:  absolute;margin-top: 114px;margin-left: 56%;">
    <div class="mast1Comment blink_me" style="margin-bottom: 22px;"></div><br><br><br>
    <div class="mast2Comment blink_me" style="margin-bottom: 28px;"></div><br><br><br>
    </div>

   

    <script src="../PROJECT/jquery.js"></script>
    <script>
    
    var callList = [];
    
    var mast1 = 0;
    localStorage.setItem('mast1', 0);

    var mast2 = 0;
    localStorage.setItem('mast2', 0);

    $('.btns').on('click', function(event){

        var caller = $(this).html().split(' ')[0];
      
        if($(this).hasClass('btnColor'))
        {
            //was not callin
            var mast1Count = parseInt(localStorage.getItem('mast1'));
            var mast2Count = parseInt(localStorage.getItem('mast2'));
            if(mast1Count <= 1){
                $('.'+caller+'1').show();
                $('.'+caller+'2').hide();

                //caller is currently using mast 1
                callList.push(caller+','+1);
            localStorage.setItem('mast1', parseInt(mast1Count+1));
            
            $(this).removeClass('btnColor');
            $(this).addClass('btnCalling');
            $(this).html(caller+' calling...');

            //show mast comment
            $('.mast1Comment').append('<div class="comment1'+caller+'">User '+caller+' is now utilizing Mast1 </div>');
            if(mast1Count > 0)
            {
                $('.maststatus1').html('<span class="blink_me">Mast 1 <b>Congested</b></span>');
            }
            //log to database
                $.ajax({
                    url: "addaction.php",
                    type: 'post',
                    data:{'caller': caller, 'mast': 'Mast 1', 'status': 'Successful', 'ifSwitch': 'No Switch', 'created_at': new Date()},
                    success: function(result){
                      //  alert(result);
                    },
                    error: function(error){
                        console.log('Could not log to database. '+error);
                    }
                });


            }
            else if(mast2Count <= 1){
                $('.'+caller+'2').show();
                $('.'+caller+'1').hide();

                // caller is currently using mast 2
                callList.push(caller+','+2);
            localStorage.setItem('mast2', parseInt(mast2Count+1));
            $(this).removeClass('btnColor');
            $(this).addClass('btnCalling');
            $(this).html(caller+' calling...');
                    
            $('.mast2Comment').append('<div class="comment2'+caller+'">User '+caller+' is now utilizing Mast2 </div>');

                var ifSwitch = "No Switch";
                console.log(mast2Count);
                if(mast2Count < 1)
                {
                    ifSwitch = "Switched from Mast 1";
                }

                if(mast2Count > 0)
                {
                    $('.maststatus2').html('<span class="blink_me">Mast 2 <b>Congested</b></span>');
                }
             //log to database
             $.ajax({
                    url: "addaction.php",
                    type: 'post',
                    data:{'caller': caller, 'mast': 'Mast 2', 'status': 'Successful', 'ifSwitch': ifSwitch, 'created_at': new Date() },
                    success: function(result){
                      //  alert(result);
                    },
                    error: function(error){
                        alert('Could not log to database. '+error);
                    }
                });

            }
            else{
                 //log to database
             $.ajax({
                    url: "addaction.php",
                    type: 'post',
                    data:{'caller': caller, 'mast': 'No Mast Assigned', 'status': 'Unsuccessful', 'ifSwitch': 'No Switch', 'created_at': new Date() },
                    success: function(result){
                        alert('All mast are in use');
                    },
                    error: function(error){
                        alert('Could not log to database. '+error);
                    }
                });
               
            }
        }
        else{
            //was calling
            $('.All'+caller).hide();

            //get which mast caller was using
            var whichMast = "";
            $.each(callList, function(count, value){
                if(value.indexOf(caller) >= 0)
                {
                    whichMast = value.split(',')[1];
                }
            });
          
            var currentMastCount = localStorage.getItem('mast'+whichMast);
            //update mast count
            localStorage.setItem('mast'+whichMast, parseInt(currentMastCount)-1);
            $(this).addClass('btnColor');
            $(this).removeClass('btnCalling');
            $(this).html(caller);
            $('.comment'+whichMast+caller).remove();
            $('.maststatus'+whichMast).html('<span class="status2">Mast '+whichMast+' Available</span>');

             $.ajax({
                    url: "addaction.php",
                    type: 'post',
                    data:{'caller': caller, 'mast': 'Unmounted from Mast '+whichMast, 'status': 'Call Ended', 'ifSwitch': 'Not Applicable', 'created_at': new Date() },
                    success: function(result){
                       // alert(result);
                    },
                    error: function(error){
                        alert('Could not log to database. '+error);
                    }
                });
        }

    })

    
    </script>

</body></html>