<?php


$servername = "localhost";
$username = "root";
$password = "";
$database = "networkcongestion";

// Create connection
$conn = new mysqli($servername, $username, $password);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//echo "Connected successfully";

$sql = $conn->query("use ".$database);

$sql = "SELECT * FROM call_register";
$result = $conn->query($sql);
$result_array = [];
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      //  echo "id: " . $row["id"]. " - Name: " . $row["firstname"]. " " . $row["lastname"]. "<br>";
      $result_array[] = $row;
    }
    echo json_encode($result_array);
} else {
    echo "0 results";
}
$conn->close();

?>