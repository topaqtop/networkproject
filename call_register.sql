-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 01, 2018 at 02:28 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `networkcongestion`
--

-- --------------------------------------------------------

--
-- Table structure for table `call_register`
--

DROP TABLE IF EXISTS `call_register`;
CREATE TABLE IF NOT EXISTS `call_register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `call_by` varchar(30) NOT NULL,
  `mast` varchar(30) NOT NULL,
  `call_status` varchar(30) NOT NULL,
  `ifSwitch` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `call_register`
--

INSERT INTO `call_register` (`id`, `call_by`, `mast`, `call_status`, `ifSwitch`, `created_at`) VALUES
(45, 'D', 'Unmounted from Mast 2', 'Call Ended', 'Not Applicable', '1970-01-01 00:00:00'),
(46, 'D', 'Mast 1', 'Successful', 'No Switch', '1970-01-01 00:00:00'),
(47, 'E', 'Unmounted from Mast 2', 'Call Ended', 'Not Applicable', '1970-01-01 00:00:00'),
(48, 'A', 'Unmounted from Mast 1', 'Call Ended', 'Not Applicable', '1970-01-01 00:00:00'),
(49, 'E', 'Mast 1', 'Successful', 'No Switch', '1970-01-01 00:00:00'),
(44, 'B', 'Unmounted from Mast 1', 'Call Ended', 'Not Applicable', '1970-01-01 00:00:00'),
(43, 'E', 'Mast 2', 'Successful', 'No Switch', '1970-01-01 00:00:00'),
(42, 'D', 'Mast 2', 'Successful', 'Switched from Mast 1', '1970-01-01 00:00:00'),
(41, 'B', 'Mast 1', 'Successful', 'No Switch', '1970-01-01 00:00:00'),
(40, 'A', 'Mast 1', 'Successful', 'No Switch', '1970-01-01 00:00:00'),
(50, 'C', 'Mast 2', 'Successful', 'Switched from Mast 1', '1970-01-01 00:00:00');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
